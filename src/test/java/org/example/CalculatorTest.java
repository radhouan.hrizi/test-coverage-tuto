package org.example;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
public class CalculatorTest {
    private final Calculator calculator = new Calculator();

    @Test
    public void testAdd() {
        assertEquals(5, calculator.add(2, 3));
        assertEquals(-1, calculator.add(-2, 1)); // test with negative numbers
        assertEquals(2, calculator.add(2, 0)); // test with zero
    }

    @Test
    public void testSubtract() {
        assertEquals(-1, calculator.subtract(2, 3));
        assertEquals(-3, calculator.subtract(-2, 1)); // test with negative numbers
        assertEquals(2, calculator.subtract(2, 0)); // test with zero
    }

    @Test
    public void testMultiply() {
        assertEquals(6, calculator.multiply(2, 3));
        assertEquals(-2, calculator.multiply(-2, 1)); // test with negative numbers
        assertEquals(0, calculator.multiply(2, 0)); // test with zero
    }

    @Test
    public void testDivide() {
        assertEquals(4, calculator.divide(8, 2));
        assertEquals(-4, calculator.divide(-8, 2)); // test with negative numbers
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDivideByZero() {
        calculator.divide(8, 0); // test division by zero
    }
}
